"""
Definition and implementation of Metadata associated with model implementations
"""
from .metadata import ModelMetadata, get_metadata