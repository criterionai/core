"""
Deep learning model and training functionality and implementations.
"""
from .metadata import ModelMetadata