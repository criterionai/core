# Brevetti AI core library
The brevettiai library enables integration with the [brevetti.ai](https://brevetti.ai) platform for development of training packages

The library is used as an API through the Job class with a number of utility functions for easy development of models.
The tools support both local development on linux and windows as well as hosted training on e.g. Amazon Web Services sagemaker platform.

## Contents
Developer documentation can be found at [docs.brevetti.ai](https://docs.brevetti.ai/) and generated [package documentation](https://s3.eu-west-1.amazonaws.com/public.data.criterion.ai/documentation/brevettiai%200.5.8/index.html) is also available.
